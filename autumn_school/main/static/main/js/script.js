const projectsNavButtons = document.querySelectorAll(".projects__nav__button");
const projectsGallery = document.querySelector(".projects__gallery");
const projectsGalleryLinks = document.querySelectorAll(".projects__gallery a");

function showProjectsLink(massiv){
    massiv.forEach((index) => {
        projectsGalleryLinks[index].style.display = "block";
        setTimeout(function(){
            projectsGalleryLinks[index].style.scale = "1";
            projectsGalleryLinks[index].style.transitionDuration = "0.5s";
        }, 500);
    })
}

function hideAllProjectsLinks(){
    projectsGalleryLinks.forEach((element) => {
        element.style.scale = "0";
        element.style.transitionDuration = "0.5s";
        setTimeout(function(){
            element.style.display = "none";
        }, 500);
    })
}

projectsNavButtons.forEach((element, i) => {
    projectsNavButtons[i].addEventListener("click", function(){
        projectsNavButtons.forEach((element, j) => {
            projectsNavButtons[j].classList.remove("projects__nav__button_active");
        })
        projectsNavButtons[i].classList.add("projects__nav__button_active");
        hideAllProjectsLinks();
        switch(i){
            case 0:
                setTimeout(function(){
                    projectsGallery.style.gridTemplateAreas = '"strizhi strizhi" "watchman ermitazh" "neptune neptune" "statistics furniture"';
                }, 500);
                let massiv = [];
                projectsGalleryLinks.forEach((element, i) => {
                    massiv.push(i);
                })
                setTimeout(function(){
                    showProjectsLink(massiv);
                }, 500);
                break;
            case 1:
                setTimeout(function(){
                    projectsGallery.style.gridTemplateAreas = '"strizhi" "neptune"';
                }, 500);
                setTimeout(function(){
                    showProjectsLink([0, 3]);
                }, 500);
                break;
            case 2:
                setTimeout(function(){
                    projectsGallery.style.gridTemplateAreas = '"watchman ermitazh"';
                }, 500);
                setTimeout(function(){
                    showProjectsLink([1, 2]);
                }, 500);
                break;
            case 3:
                setTimeout(function(){
                    projectsGallery.style.gridTemplateAreas = '"statistics furniture"';
                }, 500);
                setTimeout(function(){
                    showProjectsLink([4, 5]);
                }, 500);
                break;
        }
    })
})

const selects = document.querySelectorAll("select");

selects.forEach((element) => {
    element.addEventListener("change", function(){
        element.style.color = "#181818";
            })
})

const servicesCardsContainer = document.querySelector(".services__cards");
const servicesCards = document.querySelectorAll(".services__card");

if ((servicesCards.length % 3 != 0) && (servicesCards.length % 2 == 0)){
    servicesCardsContainer.classList.add("services__cards_two-columns");
}
else if (servicesCards.length % 3 == 1){
    servicesCards[servicesCards.length - 1].style.flexGrow = "0.5";
}

const priceFormProjectSelect = document.querySelector(".price__form__select_project");
const priceFormSizeSelect = document.querySelector(".price__form__select_size");
const priceFormButton = document.querySelector(".price__form__button");
const priceResult = document.querySelector(".price__result");
const priceSpan = document.querySelector(".price__value");
const durationSpan = document.querySelector(".duration__value");

const prices = new Map();
prices.set('Разработка сайта | Многостраничный сайт', 250000);
prices.set('Разработка сайта | Лендинг', 120000);
prices.set('Создание дизайна | Многостраничный сайт', 180000);
prices.set('Создание дизайна | Лендинг', 90000);
prices.set('Арт-дирекшн | Многостраничный сайт', 150000);
prices.set('Арт-дирекшн | Лендинг', 75000);

const durations = new Map();
durations.set('Разработка сайта | Многостраничный сайт', 20);
durations.set('Разработка сайта | Лендинг', 10);
durations.set('Создание дизайна | Многостраничный сайт', 14);
durations.set('Создание дизайна | Лендинг', 7);
durations.set('Арт-дирекшн | Многостраничный сайт', 10);
durations.set('Арт-дирекшн | Лендинг', 5);

priceFormButton.addEventListener("click", function(){
    priceFormProjectSelect.style.borderColor = "#CCCCCC";
    priceFormSizeSelect.style.borderColor = "#CCCCCC";
    event.preventDefault();
    if (priceFormProjectSelect.value != "" && priceFormSizeSelect.value != ""){
        priceSpan.textContent = prices.get(priceFormProjectSelect.value + ' | ' +  priceFormSizeSelect.value);
        durationSpan.textContent = durations.get(priceFormProjectSelect.value + ' | ' +  priceFormSizeSelect.value)
        priceResult.classList.add("price__result_active");
    }
    else{
        if (priceFormProjectSelect.value == ""){
            priceFormProjectSelect.style.borderColor = "#FF0000";
        }
        if (priceFormSizeSelect.value == ""){
            priceFormSizeSelect.style.borderColor = "#FF0000";
        }
    }
    console.log(priceFormProjectSelect.value);
    
})

const qnaCards = document.querySelectorAll(".qna__card");
const qnaCardButtons = document.querySelectorAll(".qna__card__button");
const qnaCardTexts = document.querySelectorAll(".qna__card__text");

qnaCards.forEach((element, i) => {
    qnaCards[i].addEventListener("click", function(){
        let wasOpened = qnaCardButtons[i].classList.contains("qna__card__button_opened");
        qnaCards.forEach((element, j) => {
            qnaCardButtons[j].classList.remove("qna__card__button_opened");
            qnaCardTexts[j].classList.remove("qna__card__text_opened");
        })
        if (! wasOpened){
            setTimeout(function(){
                qnaCardButtons[i].classList.add("qna__card__button_opened");
                qnaCardTexts[i].classList.add("qna__card__text_opened");
            }, 300)
        }
    })
})