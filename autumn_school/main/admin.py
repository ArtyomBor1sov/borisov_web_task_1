from django.contrib import admin
from .models import Service, Response, Price

# Register your models here.
admin.site.register(Service)
admin.site.register(Response)
admin.site.register(Price)