from django.db import models

# Create your models here.
class Service(models.Model):
    title = models.CharField('Услуга', max_length=50)
    description = models.TextField('Описание', max_length=250)
    price = models.IntegerField('Цена')

    def __str__(self):
        return self.title

class Price(models.Model):
    PROJECT_TYPE_CHOISES = [
        (None, 'Тип работы'),
        ('Разработка сайта', 'Разработка сайта'),
        ('Создание дизайна', 'Создание дизайна'),
        ('Арт-дирекшн', 'Арт-дирекшн')
    ]
    project = models.CharField(
        'Тип проекта',
        max_length=50,
        choices=PROJECT_TYPE_CHOISES,
        default='Тип работы',
    )
    PROJECT_SIZE_CHOISES = [
        (None, 'Насколько большой проект?'),
        ('Многостраничный сайт', 'Многостраничный сайт'),
        ('Лендинг', 'Лендинг'),
    ]
    size = models.CharField(
        'Размер проекта',
        max_length=50,
        choices=PROJECT_SIZE_CHOISES,
        default='Насколько большой проект?',
    )
    price = models.IntegerField()

    def __str__(self):
        return self.project + ' | ' + self.size

class Response(models.Model):
    name = models.CharField('Имя', max_length=50)
    email = models.CharField('Почта', max_length=50)
    PROJECT_TYPE_CHOISES = [
        (None, 'Услуга'),
        ('Разработка сайта', 'Разработка сайта'),
        ('Создание дизайна', 'Создание дизайна'),
        ('Арт-дирекшн', 'Арт-дирекшн')
    ]
    project = models.CharField(
        'Тип проекта',
        max_length=50,
        choices=PROJECT_TYPE_CHOISES,
        default='Услуга',
    )
    comment = models.TextField('Комментарий', max_length=500, blank=True)

    def __str__(self):
        return self.project + ' | ' + self.name + ' | ' + self.email

class Prices(models.Model):
    PROJECT_TYPE_CHOISES = [
        (None, 'Тип работы'),
        ('Разработка сайта', 'Разработка сайта'),
        ('Создание дизайна', 'Создание дизайна'),
        ('Арт-дирекшн', 'Арт-дирекшн')
    ]
    project = models.CharField(
        max_length=50,
        choices=PROJECT_TYPE_CHOISES,
        default='Тип работы',
    )
    comment = models.TextField(max_length=500)

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = 'Response'
        verbose_name_plural = 'Responses'