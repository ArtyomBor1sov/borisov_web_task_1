from django.shortcuts import render
from django.core import serializers
from .models import Service, Price
from .forms import ResponseForm, PriceForm

# Create your views here.
def index(request):
    if request.method == 'POST':
        # if 'price_button' in request.POST:
        #     price_form = PriceForm(request.POST)
        #     if price_form.is_valid():
        #         result = Price.objects.all().filter(project='Создание дизайна')
        if 'response_button' in request.POST:
            response_form = ResponseForm(request.POST)
            if response_form.is_valid():
                response_form.save()
    services = Service.objects.all()
    json_serializer = serializers.get_serializer("json")()
    options = json_serializer.serialize(Price.objects.all(), ensure_ascii=False)
    price_form = PriceForm()
    # result = Price.objects.all().filter(project='Разработка сайта')
    # price = 0
    # duration = 0
    response_form = ResponseForm()
    data = {
        'services': services, 
        'price_form': price_form,
        # 'result': result,
        # 'price': price,
        # 'duration': duration,
        'response_form': response_form 
    }
    return render(request, 'main/index.html', data)