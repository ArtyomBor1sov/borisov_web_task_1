from .models import Response, Price
from django.forms import ModelForm, TextInput, Textarea, Select, EmailInput

class PriceForm(ModelForm):

    class Meta:
        model = Price
        fields = ['project', 'size', 'price']
        widgets = {
            'project': Select(attrs={
                'class': 'price__form__select_project', 
            }), 
            'size': Select(attrs={
                'class': 'price__form__select_size'
            })
        }

class ResponseForm(ModelForm):

    class Meta:
        model = Response
        fields = ['name', 'email', 'project', 'comment']
        widgets = {
            'name': TextInput(attrs={
                'placeholder': 'Имя'
            }),
            'email': EmailInput(attrs={
                'placeholder': 'Почта'
            }),
            'comment': Textarea(attrs={
                'placeholder': 'Комментарий', 
            })
        }

